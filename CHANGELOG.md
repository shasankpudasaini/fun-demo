# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.4.2](https://gitlab.com/surajmaharjan/fun-demo/compare/v1.4.1...v1.4.2) (2022-03-02)

### [1.4.1](https://gitlab.com/surajmaharjan/fun-demo/compare/v1.4.0...v1.4.1) (2022-03-02)


### Bug Fixes

* somefix related to rabbit mq sent ([6ed0dc1](https://gitlab.com/surajmaharjan/fun-demo/commit/6ed0dc162e17d0674f2c32e20ce1ea4da891a97f))

## [1.4.0](https://gitlab.com/surajmaharjan/fun-demo/compare/v1.3.0...v1.4.0) (2022-03-02)


### Features

* rabbit mq implementation done ([b5ac659](https://gitlab.com/surajmaharjan/fun-demo/commit/b5ac6593c4ed2c8ccaa7c03e29c6e7c2a7a7081f))
* Rabbit MQ Setup done ([f7dda4a](https://gitlab.com/surajmaharjan/fun-demo/commit/f7dda4aa5faa287047f6b281385d63ac5d1c7f6e))


### Bug Fixes

* removed jpa auditing currently causing issue in unit testing and also added comment for the JPATests ([0593723](https://gitlab.com/surajmaharjan/fun-demo/commit/0593723e9e868817a31025ad91eabb31ff24b9ce))

## [1.3.0](https://gitlab.com/surajmaharjan/fun-demo/compare/v1.2.0...v1.3.0) (2022-03-01)


### Features

* Done changes in FunDto.class file ([2388fc7](https://gitlab.com/surajmaharjan/fun-demo/commit/2388fc7e802f6f64e86a75ad87ab8a6a9a5da11f))


### Bug Fixes

* Remove unnecessary id ([f4f99fb](https://gitlab.com/surajmaharjan/fun-demo/commit/f4f99fb53e0a260beee7f46cd5a0c9eff378a88c))
* Response message changed ([8cd8751](https://gitlab.com/surajmaharjan/fun-demo/commit/8cd875175933eb344c59549fc06eefd791d906d2))
* something changed ([8723771](https://gitlab.com/surajmaharjan/fun-demo/commit/87237714460d512a055991539ade67ec7cbed4de))

## [1.2.0](https://gitlab.com/surajmaharjan/fun-demo/compare/v1.1.1...v1.2.0) (2022-03-01)


### Features

* feature added ([f3c7044](https://gitlab.com/surajmaharjan/fun-demo/commit/f3c7044f9a6fd702aa2e417c0457bb951e2ee284))

### 1.1.1 (2022-03-01)
