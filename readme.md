# Junit & Mockito

* Prerequisite
    
       Good understanding of Java
       
       Knowledge of RESTful Web Services
        
       Knowledge of Maven
        
       Knowledge of Spring Boot
       
* Tools Needed
  
    Java Development Kit (JDK) 1.8+
  
    IntelliJ IDEA, Eclipse, NetBeans or Spring Tool Suite
    
    
* **Importance of software testing, Test Driven Development (TDD) basics, and aspects of test cases**
 
  * Test Case/s is a specific set of instructions that the tester is expected to follow to achieve a specific output. Test cases are documented keeping in mind the requirements provided by the client. The key purpose of a test case is to ensure if different features within an application are working as expected. It helps the tester, validate if the software is free of defects and if it is working as per the expectations of the end-users. Other benefits of test cases include:
  
  * Test cases ensure good test coverage
  * Help improve the quality of software
  * Decreases the maintenance and software support costs
  * Help verify that the software meets the end-user requirements
  * Unit tests are a kind of living documentation of the product. To learn what functionality is provided by one module or another, developers can refer to unit tests to get a basic picture of the logic of the module and the system as a whole
  * Allows the tester to think thoroughly and approach the tests from as many angles as possible
  * Test cases are reusable for the future — anyone can reference them and execute the test.
  * Best practices suggest that developers first run all unit tests or a group of tests locally to make sure any coding changes don’t disrupt the existing code.
  * However, consider the human factor: A developer might forget to run unit tests after making changes and submit potentially non-working code to a common branch. To avoid this, many companies apply a continuous development approach. Tools for continuous integration are used for this, allowing developers to run unit tests automatically. Thus, any unwanted changes in the code will be detected by a cold, logical machine.
  
 * **Unit Testing Frameworks in Java**
 
        we will use Junit and Mockito for the unit tests.
        
        JUnit is an open source unit testing framework for Java. This framework is commonly used to write unit tests for Spring Boot applications.
        
        Mockito is one of the most popular mocking frameworks for Java. You can create mock objects and determine the behaviour of these mock objects. For instance, you can create a mock server object and configure the behaviour of this server according to your unit testing needs. Mockito greatly simplifies the implementation of unit tests for the classes with external dependencies .
        
        we will use Junit to write repeatable unit tests and Mockito will be used for mocking external dependencies.
        
 
 * **Common annotations and meanings**
     
    * @Test
     
    * @ParameterizedTest	
    * @RepeatedTest
    * @Before	
    * @BeforeClass	
    * @After	
    * @AfterClass
    * @DisplayName	
    * @BeforeEach
    * @AfterEach
    * @BeforeAll
    * @AfterAll
    * @Tag
    * @Disabled
           
        Look the link below for detailed descripion
        
        https://devqa.io/junit-5-annotations/
