package com.junitdemo.junits.component;

import com.junitdemo.junits.dto.FunDto;
import com.junitdemo.junits.dto.FunDtoError;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CustomValidator {


    public FunDtoError validateFunDto(FunDto funDto) {
        boolean hasError = false;
        FunDtoError funDtoError = new FunDtoError();
        Map<String, List<String>> errorMap = new HashMap<>();
        List<String> errorList = validateFunText1(funDto.getFunText1());
        if (!errorList.isEmpty()) {
            hasError = true;
            errorMap.put("funText1", errorList);
        }
        funDtoError.setErrorMessages(errorMap);

        funDtoError.setHasError(hasError);
        return funDtoError;
    }

    private List<String> validateFunText1(String funText) {
        List<String> errorList = new ArrayList<>();

        if (funText == null) {
            errorList.add("Fun Text is null");
        }
        if (funText != null && funText.isEmpty()) {
            errorList.add("Fun Text is blank");
        }

        if (funText != null && (funText.length() > 10 || funText.length() < 3)) {
            errorList.add("Fun Text size is invalid");
        }
        return errorList;
    }
}


