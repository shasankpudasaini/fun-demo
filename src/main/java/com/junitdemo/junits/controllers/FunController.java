package com.junitdemo.junits.controllers;


import com.junitdemo.junits.dto.FunDto;
import com.junitdemo.junits.dto.apiresponse.FunResponseDto;
import com.junitdemo.junits.service.FunService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/fun")
public class FunController {

    private final FunService funService;

    public FunController(FunService funService) {
        this.funService = funService;
    }

    @PostMapping
    public ResponseEntity<FunResponseDto> save(@RequestBody FunDto funDto) {
        return new ResponseEntity<>(FunResponseDto
                .builder()
                .status(true)
                .message("Fun Created")
                .data(funService.createFun(funDto))
                .build(), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<FunResponseDto> findById(@PathVariable("id") Integer id) {
        FunDto funDto = funService.findById(id);
        if (funDto != null)
            return new ResponseEntity<>(FunResponseDto
                    .builder()
                    .status(true)
                    .message("Fun Fetched")
                    .data(funDto)
                    .build(), HttpStatus.OK);
        else
            return new ResponseEntity<>(FunResponseDto
                    .builder()
                    .status(false)
                    .message("Fun Not Found")
                    .data(null)
                    .build(), HttpStatus.NOT_FOUND);

    }

    /**
     * This is api that gives back list
     *
     * @return
     */
    @GetMapping("/list")
    public ResponseEntity<List<FunDto>> findFunDtoList() {
        List<FunDto> funDtoList = funService.findAll();
        if (!funDtoList.isEmpty())
            return new ResponseEntity(funDtoList, HttpStatus.OK);
        else
            return new ResponseEntity(new ArrayList<>(), HttpStatus.NOT_FOUND);
    }

    @GetMapping
    public ResponseEntity<FunResponseDto> findAll() {
        return new ResponseEntity(FunResponseDto
                .builder()
                .status(true)
                .message("Fun Fetched Successfully")
                .data(funService.findAll())
                .build(), HttpStatus.OK);
    }
}
