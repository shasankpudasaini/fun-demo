package com.junitdemo.junits.controllers.base;

import com.junitdemo.junits.dto.apiresponse.FunResponseDto;

public class BaseController {

    public FunResponseDto successResponse(String message, Object data) {
        return FunResponseDto.builder()
                .status(true)
                .message(message)
                .data(data)
                .build();
    }

    public FunResponseDto errorResponse(String message, Object data) {
        return FunResponseDto.builder()
                .status(false)
                .message(message)
                .data(data)
                .build();
    }
}
