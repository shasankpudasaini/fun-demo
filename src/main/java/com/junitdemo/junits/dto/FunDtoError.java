package com.junitdemo.junits.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FunDtoError {

    private Boolean hasError;

    private Map<String, List<String>> errorMessages;
}
