package com.junitdemo.junits.entity;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "tbl_fun", uniqueConstraints = {
        @UniqueConstraint(name = "unique_funtext1", columnNames = "fun_text1")
})
public class Fun {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "fun_text1")
    private String funText1;

    @Column(name = "fun_text2", length = 10)
    private String funText2;
}
