package com.junitdemo.junits.rapipay.mq;//package com.springbatch.infodev.mq;
//
//import com.springbatch.infodev.mq.service.RabbitMQProducer;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.Date;
//
//@RestController
//public class TestController {
//    private final RabbitMQProducer rabbitMQProducer;
//
//    public TestController(RabbitMQProducer rabbitMQProducer) {
//        this.rabbitMQProducer = rabbitMQProducer;
//    }
//
//    @GetMapping("/send")
//    public ResponseEntity<?> send() {
//        Mail mail = Mail.builder()
//                .to("lightsuraj129@gmail.com")
//                .subject("A test mail from RabbitMQ.")
//                .body("Sample mail " + new Date())
//                .build();
//        for (int i = 1; i <= 10; i++) {
//            mail.setId(i);
//            rabbitMQProducer.produceMessage(mail);
//        }
//        return ResponseEntity.ok("Mail is sending to all the receivers.");
//    }
//}
