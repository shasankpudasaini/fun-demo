package com.junitdemo.junits.rapipay.mq.components;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.junitdemo.junits.dto.apiresponse.FunResponseDto;
import com.junitdemo.junits.rapipay.mq.dto.Message;
import com.junitdemo.junits.rapipay.mq.entity.MyRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class RestCallManager {

    private final ObjectMapper objectMapper;

    public RestCallManager(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public String makeRestCall(Message message) {
        try {
            Integer id = message.getMId();
            String messageJson = message.getMessageJson();
            MyRequest myRequest = objectMapper.readValue(messageJson, MyRequest.class);
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
            httpHeaders.addAll(getHeaderData());

            AuthorDto body = AuthorDto.builder()
                    .email(myRequest.getName())
                    .name(myRequest.getDetails())
                    .mobileNumber(String.valueOf(id))
                    .build();

            ResponseEntity responseEntity =
                    restTemplate.exchange("http://localhost:8888/author/create",
                            HttpMethod.POST, new HttpEntity<>(body, httpHeaders), String.class);

            /**
             * Expected Header's 200 , 400
             */
            HttpStatus httpStatus = responseEntity.getStatusCode();
            if (httpStatus == HttpStatus.OK) {
                /**
                 * Check if the response that we are getting is successful or not
                 * If getting 200 okay also check the error code
                 */
                FunResponseDto funResponseDto = objectMapper.readValue(String.valueOf(responseEntity.getBody()), FunResponseDto.class);
                if (funResponseDto.getStatus()) {
                    return String.valueOf(responseEntity.getBody());
                } else {
                    if (String.valueOf(funResponseDto.getData()).equals("ERROR_CODE_001")) {
                        log.info("Obtained Response from another service : {} ",funResponseDto.toString());
                        throw new RuntimeException("Retrying again");
                    }
                }
            }
            return null;
        } catch (Exception ex) {
            if (ex instanceof ResourceAccessException) {
                ResourceAccessException exception = (ResourceAccessException) ex;
                log.error("ERROR >>>>>>>>> ResourceAccessException {} ", exception.getMessage());
                /**
                 * This is retryable
                 * This is transient error
                 * We can identify transient and permanent error gradually
                 * We can capture this error and store it somewhere
                 */
                throw new RuntimeException("Retrying again");
            } else if (ex instanceof HttpClientErrorException) {
                HttpClientErrorException exception = (HttpClientErrorException) ex;
                log.error("ERROR >>>>>>>>> HttpClientErrorException {} ", exception.getMessage());
                /**
                 * This is not retryable
                 */
                log.info("THIS IS PERMANENT ERROR !!! ");
            } else if (ex instanceof HttpServerErrorException) {
                HttpServerErrorException exception = (HttpServerErrorException) ex;
                log.error("ERROR >>>>>>>>> HttpServerErrorException {} ", exception.getMessage());
                log.info("THIS IS PERMANENT ERROR !!! ");
            }
            ex.printStackTrace();
            throw new RuntimeException("Retrying again");
        }
    }

    private MultiValueMap<String, String> getHeaderData() {
        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
        multiValueMap.add("secret-key", "APPLE@123");
        return multiValueMap;
    }
}
