package com.junitdemo.junits.rapipay.mq.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "tbl_request", uniqueConstraints = {

})
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class MyRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "details", columnDefinition = "TEXT")
    private String details;

    public MyRequest(Integer id) {
        this.id = id;
    }
}
