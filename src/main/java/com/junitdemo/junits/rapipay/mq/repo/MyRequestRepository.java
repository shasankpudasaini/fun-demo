package com.junitdemo.junits.rapipay.mq.repo;

import com.junitdemo.junits.rapipay.mq.entity.MyRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MyRequestRepository extends JpaRepository<MyRequest, Integer> {
}
