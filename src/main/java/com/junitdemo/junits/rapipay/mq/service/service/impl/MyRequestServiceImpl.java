package com.junitdemo.junits.rapipay.mq.service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.junitdemo.junits.rapipay.mq.dto.Message;
import com.junitdemo.junits.rapipay.mq.entity.MyRequest;
import com.junitdemo.junits.rapipay.mq.repo.MyRequestRepository;
import com.junitdemo.junits.rapipay.mq.service.mqservice.RabbitMQProducer;
import com.junitdemo.junits.rapipay.mq.service.service.MyRequestService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyRequestServiceImpl implements MyRequestService {

    private final MyRequestRepository myRequestRepository;
    private final RabbitMQProducer rabbitMQProducer;
    private final ObjectMapper objectMapper;

    public MyRequestServiceImpl(MyRequestRepository myRequestRepository,
                                RabbitMQProducer rabbitMQProducer,
                                ObjectMapper objectMapper) {
        this.myRequestRepository = myRequestRepository;
        this.rabbitMQProducer = rabbitMQProducer;
        this.objectMapper = objectMapper;
    }

    @Override
    public MyRequest save(MyRequest myRequest) throws JsonProcessingException {
        myRequest = myRequestRepository.save(myRequest);
        if (myRequest != null) {
            Message message = Message
                    .builder()
                    .mId(myRequest.getId())
                    .messageJson(objectMapper.writeValueAsString(myRequest))
                    .build();
            rabbitMQProducer.produceMessage(message);
        }
        return myRequest;
    }

    @Override
    public List<MyRequest> findAll() {
        return null;
    }
}
