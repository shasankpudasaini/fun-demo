package com.junitdemo.junits.rapipay.mq.service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.junitdemo.junits.rapipay.mq.components.RestCallManager;
import com.junitdemo.junits.rapipay.mq.dto.Message;
import com.junitdemo.junits.rapipay.mq.entity.MyRequest;
import com.junitdemo.junits.rapipay.mq.entity.MyResponse;
import com.junitdemo.junits.rapipay.mq.repo.MyResponseRepository;
import com.junitdemo.junits.rapipay.mq.service.service.MyResponseService;
import org.springframework.stereotype.Service;

@Service
public class MyResponseServiceImpl implements MyResponseService {

    private final MyResponseRepository myResponseRepository;
    private final RestCallManager restCallManager;

    public MyResponseServiceImpl(MyResponseRepository myResponseRepository,
                                 RestCallManager restCallManager) {
        this.myResponseRepository = myResponseRepository;
        this.restCallManager = restCallManager;
    }

    /**
     * @param message
     */
    @Override
    public void processMessageFromQueue(Message message) {
        String jsonString = restCallManager.makeRestCall(message);
        MyResponse myResponse = MyResponse
                .builder()
                .apiResponse(jsonString)
                .myRequest(new MyRequest(message.getMId()))
                .build();

        save(myResponse);
    }

    @Override
    public MyResponse save(MyResponse myResponse) {
        return myResponseRepository.save(myResponse);
    }
}
