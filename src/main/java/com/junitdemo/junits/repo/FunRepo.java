package com.junitdemo.junits.repo;

import com.junitdemo.junits.entity.Fun;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FunRepo extends JpaRepository<Fun, Integer> {
}
