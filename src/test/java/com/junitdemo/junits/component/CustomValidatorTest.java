package com.junitdemo.junits.component;

import com.junitdemo.junits.dto.FunDto;
import com.junitdemo.junits.dto.FunDtoError;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.mockito.InjectMocks;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


@Slf4j
@WebMvcTest(CustomValidator.class)
class CustomValidatorTest {

    @InjectMocks
    private CustomValidator customValidator;

    private FunDto funDto1;
    private FunDto funDto2;

    @BeforeEach
    void setUp(TestInfo testinfo) {
        log.info("test {}");
        funDto1 = FunDto.builder()
                .funText1("this is random text")
                .funText2("")
                .build();

        funDto2 = FunDto.builder()
                .funText1(null)
                .funText2("")
                .build();
    }

    @Test
    void validateFunDtoErrorContainsFunText1() {
        FunDtoError funDtoError = customValidator.validateFunDto(funDto1);
        Assertions.assertTrue(funDtoError.getErrorMessages().containsKey("funText1"));
    }

    @Test
    void validateMessages() {
        FunDtoError funDtoError = customValidator.validateFunDto(funDto2);
        Assertions.assertTrue(funDtoError.getErrorMessages().get("funText1").contains("Fun Text is null"));
    }
}