package com.junitdemo.junits.repo;

import com.junitdemo.junits.entity.Fun;
import lombok.extern.slf4j.Slf4j;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runners.MethodSorters;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;


@Slf4j
@ExtendWith(MockitoExtension.class)
/**
 * @DataJpaTest is used to test JPA repositories. It is used in combination with @RunWith(SpringRunner. class) .
 * The annotation disables full auto-configuration and applies only configuration relevant to JPA tests.
 */

/**
 * NOTE FOR WHICH DATABASE TO CHOOSE DURING TEST
 * So, if you want to test with postgres or mysql, replace your test as follows:
 *
 * @DataJpaTest
 * @AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
 * public class FunRepoTest {
 *
 * }
 * If you want to use an in-memory database  [H2] for those tests,
 * you need to add one to the test classpath. Add this to your pom file

 *         <dependency>
 *             <groupId>com.h2database</groupId>
 *             <artifactId>h2</artifactId>
 *             <version>2.1.210</version>
 *             <scope>test</scope>
 *         </dependency>
 * testCompile('com.h2database:h2')
 */
@DataJpaTest
/**
 * @FixMethodOrder
 * This class allows the user to choose the order of execution of the methods within a test class.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FunRepoTest {
    @Autowired
    private FunRepo funRepo;

    /**
     * This beforeEachCaseStarts function is executed before every test case
     * TestInfo interface is used to capture the displayName , tag etc of the test cases
     * The displayName helps to identify the test case
     * <p>
     * Here we are ignoring persisting to database  for test with displayName [optionalTester]
     *
     * @param testInfo
     */
    @BeforeEach
    void beforeEachCaseStarts(TestInfo testInfo) {
        if (testInfo.getDisplayName().equalsIgnoreCase("optionalTester")) {
            log.info("++++++++ SKIPPING BEFORE EACH FOR optionalChecker");
        } else {
            log.info(">>>> creating elements in database from beforeEachCaseStarts");

            List<Fun> funList = Arrays.asList(
                    new Fun(null, "funText1", "funText21"),
                    new Fun(null, "funText2", "funText22")
            );
            funRepo.saveAll(funList);
        }
    }

    /**
     * This afterEach function is executed after every test case
     * TestInfo interface is used to capture the displayName , tag etc of the test cases
     * The displayName helps to identify the test case
     *
     * @param testInfo
     */
    @AfterEach
    public void destroyAll(TestInfo testInfo) {
        log.info(">>>> Destroying all elements stored in database from destroyAll");
        funRepo.deleteAll();
    }

    /**
     * Display name is the name of the test.
     * This will be used in AfterEach or BeforeEach to handle different Use Cases
     */
    @Test
    @DisplayName("findAllTester")
    void findAllTest() {
        List<Fun> funList = funRepo.findAll();
        Assertions.assertEquals(funList.size(), 2);
    }


    @Test
    void checkForOptionalPresent() {
        Optional<Fun> optionalFun = funRepo.findById(1);
        Assertions.assertTrue(optionalFun.isPresent());
    }

    @DisplayName("optionalTester")
    @Test
    void checkForOptionalEmpty() {
        Optional<Fun> optionalFun = funRepo.findById(1);
        Assertions.assertFalse(optionalFun.isPresent());
    }

    @Test
    @DisplayName("saveAllTester")
    void saveAll() {
        List<Fun> funList = Arrays.asList(
                new Fun(null, "funText11", "funText21"),
                new Fun(null, "funText111", "funText22"),
                new Fun(null, "funText1111", "funText23")
        );

        Iterable<Fun> funListIterable = funRepo.saveAll(funList);

        AtomicInteger validIdFound = new AtomicInteger();
        funListIterable.forEach(fun -> {
            if (fun.getId() > 0) {
                validIdFound.getAndIncrement();
            }
        });
        Assertions.assertEquals(validIdFound.intValue(), 3);
    }


    /**
     * @Disabled annotation with message
     * This is used if you want to ignore the test during build
     * We need to ignore if this is incomplete or having issues which will not effect the system
     */
    @Disabled("This is not yet completed \nSoon it will be completed")
    @Test
    void handleUniqueConstraintFailure() {
        Fun fun = new Fun(null, "funText1", "funText21");

        Exception exception = Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
            funRepo.save(fun);
        });

        String expectedMessage = "could not execute statement";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }
}